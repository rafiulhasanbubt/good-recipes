//
//  Recipe.swift
//  RecipeGoodEatins
//
//  Created by rafiul hasan on 12/28/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import Foundation

struct  Recipe {
    let title: String
    let instructions: String
    let imageName: String
}
