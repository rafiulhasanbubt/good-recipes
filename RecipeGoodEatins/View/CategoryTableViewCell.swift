//
//  CategoryTableViewCell.swift
//  RecipeGoodEatins
//
//  Created by rafiul hasan on 12/28/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        categoryImageView.layer.cornerRadius = 10.0
        self.layer.cornerRadius = 10.0
    }

    func configureCell(category: FoodCategory){
        categoryImageView.image = UIImage(named: category.imageName)
        categoryNameLabel.text = category.title
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
