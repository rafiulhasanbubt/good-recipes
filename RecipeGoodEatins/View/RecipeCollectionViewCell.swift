//
//  RecipeCollectionViewCell.swift
//  RecipeGoodEatins
//
//  Created by rafiul hasan on 12/28/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class RecipeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        recipeImageView.layer.cornerRadius = 10.0
        self.layer.cornerRadius = 10.0
    }
    
    func configureCell(recipe: Recipe) {
        recipeImageView.image = UIImage(named: recipe.imageName)
        recipeTitle.text = recipe.title
    }
}
