//
//  RecipeDetailViewController.swift
//  RecipeGoodEatins
//
//  Created by rafiul hasan on 12/28/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class RecipeDetailViewController: UIViewController {

    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var recipeInstructionsLabel: UILabel!
    
    var selectedRecipe: Recipe!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        recipeImageView.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
        recipeImageView.image = UIImage(named: selectedRecipe.imageName)
        recipeNameLabel.text = selectedRecipe.title
        recipeInstructionsLabel.text = selectedRecipe.instructions
        
        title = selectedRecipe.title
    }
    
}
