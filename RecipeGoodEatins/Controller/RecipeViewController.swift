//
//  RecipeViewController.swift
//  RecipeGoodEatins
//
//  Created by rafiul hasan on 12/28/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var recipeCollectionView: UICollectionView!
    
    var selectedCategory: String!
    var recipes: [Recipe]!
    let data = DataSet()
    var recipeToPass: Recipe!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        recipeCollectionView.delegate = self
        recipeCollectionView.dataSource = self
        
        recipes = data.getRecipes(forCategoryTitle: selectedCategory)
        title = selectedCategory
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeCell", for: indexPath) as? RecipeCollectionViewCell
        let recipe = recipes[indexPath.item]
        cell?.configureCell(recipe: recipe)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.bounds.width
        let cellDimention = (width/2) - 16
        return CGSize(width: cellDimention, height: cellDimention)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        recipeToPass = recipes[indexPath.item]
        
        let controller = storyboard?.instantiateViewController(identifier: "RecipeDetailViewController") as! RecipeDetailViewController
        controller.selectedRecipe = recipeToPass
        self.navigationController?.pushViewController(controller, animated: true)
        //self.present(controller, animated: true, completion: nil)
    }

}
